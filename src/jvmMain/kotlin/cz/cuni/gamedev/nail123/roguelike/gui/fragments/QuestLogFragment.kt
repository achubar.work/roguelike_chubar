package cz.cuni.gamedev.nail123.roguelike.gui.fragments

import cz.cuni.gamedev.nail123.roguelike.GameConfig
import cz.cuni.gamedev.nail123.roguelike.world.World
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom
import org.hexworks.zircon.api.Components
import org.hexworks.zircon.api.component.Fragment

class QuestLogFragment(val world: World): Fragment {
    val questLogBox = Components.paragraph()
        .withSize(GameConfig.SIDEBAR_WIDTH - 2, 8)
        .build()

//    val selectedIndexProperty = createPropertyFrom(-1)
//    var selectedIndex
//        get() = selectedIndexProperty.value
//        set(value) { selectedIndexProperty.value = value }

    override val root = Components.vbox()
        .withSize(GameConfig.SIDEBAR_WIDTH - 2, 10)
        .withPosition(0, 20)
        .withSpacing(0)
        .build().apply {
            addComponent(Components.header().withText("Quest Log").build())
            addComponent(questLogBox)
        }

    fun update() {
        val questLog = world.player.questLog
        if (questLog.quests.isEmpty()) {
            questLogBox.text = "Empty"
        } else {
            questLogBox.text = questLog.quests.mapIndexed {i, quest ->
                i;
                val name = quest.name
                val progress = quest.progress
                "$name \n $progress"
            }.joinToString("\n \n")
        }
    }
}