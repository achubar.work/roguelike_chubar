package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Sword(val attackPower: Int, val leechOnKill: Double): Weapon(GameTiles.SWORD) {
    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.attack += attackPower
            character.leechOnKill += leechOnKill
        }
    }

    override fun onUnequip(character: HasInventory) {
        if (character is Player) {
            character.attack -= attackPower
            character.leechOnKill -= leechOnKill
        }
    }

    override fun toString(): String {
        var leech = (leechOnKill * 100).toInt()
        return "Sword($attackPower, $leech%)"
    }
}