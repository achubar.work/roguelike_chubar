package cz.cuni.gamedev.nail123.roguelike.mechanics

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import kotlin.math.ceil
import kotlin.random.Random.Default.nextDouble

object Combat {
    fun attack(attacker: HasCombatStats, defender: HasCombatStats) {
        var damage = Math.max(attacker.attack - defender.defense, 0)
        if (nextDouble() < 0.1) {
            damage *= 2
            attacker.logMessage("Critical hit!")
        }
        defender.takeDamage(damage)
        if (defender.hitpoints <= 0) // Adding healing on kill
        {
            attacker.heal(Math.ceil(defender.maxHitpoints * attacker.leechOnKill).toInt())
        }
        if (attacker is Player) {
            attacker.logMessage(String.format("You hit %s for %d damage!", defender, damage))
        } else if (defender is Player) {
            attacker.logMessage(String.format("%s hits you for %d damage!", attacker, damage))
        } else {
            attacker.logMessage(String.format("%s hits %s for %d damage!", attacker, defender, damage))
        }
    }
}