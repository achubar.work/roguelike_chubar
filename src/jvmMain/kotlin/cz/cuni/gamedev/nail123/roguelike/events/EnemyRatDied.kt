package cz.cuni.gamedev.nail123.roguelike.events

import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat

class EnemyRatDied(override val emitter: Rat): GameEvent() {}