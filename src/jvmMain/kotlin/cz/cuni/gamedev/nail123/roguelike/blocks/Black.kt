package cz.cuni.gamedev.nail123.roguelike.blocks

import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Black: GameBlock(GameTiles.BLACK){
    override val blocksMovement = true
}