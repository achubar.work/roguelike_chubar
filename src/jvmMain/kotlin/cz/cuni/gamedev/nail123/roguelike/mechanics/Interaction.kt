package cz.cuni.gamedev.nail123.roguelike.mechanics

import cz.cuni.gamedev.nail123.roguelike.entities.NPCs.QuestGiver
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasQuestLog
import cz.cuni.gamedev.nail123.roguelike.entities.quests.Quest
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import kotlin.random.Random.Default.nextDouble

object Interaction {
    fun takeQuest(taker: HasQuestLog, giver: QuestGiver) { // Both for take and complete
        if (giver.hasQuest() && taker.quests.isEmpty())
        {
            taker.addQuest(giver.giveQuest()) // Log message is in the taker method
        }
        else if (!taker.quests.isEmpty() && taker.quests[0].completed) // Bad for multiple quests
        {
            giver.giveReward(taker.questLog.removeAt(0));

            taker.logMessage(String.format("Thanks for the help. Here is your reward!"))
        }
        else if (!giver.hasQuest())
        {
            taker.logMessage(String.format("I don't have any quests for you"))
        }
        else
        {
            taker.logMessage(String.format("I cannot give you more quests right now"))
        }
    }
}
