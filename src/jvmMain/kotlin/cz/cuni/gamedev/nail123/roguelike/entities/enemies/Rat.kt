package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.events.EnemyOrcDied
import cz.cuni.gamedev.nail123.roguelike.events.EnemyRatDied
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Rat(level: Int): Enemy(GameTiles.RAT), HasSmell {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 7 + level/4
    override var healOnLevel = 0

    override val maxHitpoints = 10 + level * 2
    override var hitpoints = 10 + level * 2
    override var attack = 2 + level
    override var defense = 0
    override var leechOnKill= 0.0

    override fun update() {
        if (Pathfinding.chebyshev(position, area.player.position) <= smellingRadius) {
            goBlindlyTowards(area.player.position)
        }
    }

    override fun die() {
        super.die()
        EnemyRatDied(this).emit()

    }
}