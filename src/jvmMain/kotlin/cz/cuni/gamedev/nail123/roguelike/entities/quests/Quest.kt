package cz.cuni.gamedev.nail123.roguelike.entities.quests

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item

abstract class Quest(reward: Item) : GameEntity() {
    var isTaken: Boolean = false

    override val blocksMovement: Boolean
        get() = false
    override val blocksVision: Boolean
        get() = false
    abstract val name: String?
    abstract val progress: String?

    abstract val reward: Item

    var completed = false
}