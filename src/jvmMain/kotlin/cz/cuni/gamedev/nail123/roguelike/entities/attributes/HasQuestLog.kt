package cz.cuni.gamedev.nail123.roguelike.entities.attributes

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.quests.KillEnemies
import cz.cuni.gamedev.nail123.roguelike.entities.quests.Quest
import cz.cuni.gamedev.nail123.roguelike.events.QuestLogUpdated
import cz.cuni.gamedev.nail123.roguelike.events.logMessage


interface HasQuestLog {
    val questLog: QuestLog

    val quests: List<Quest>
        get() = questLog.quests

    fun addQuest(quest: Quest) = questLog.add(quest)
}

class QuestLog(val player: Player) {

    private var _quests = ArrayList<Quest>()
    val quests: ArrayList<Quest>
        get() = _quests


    fun add(quest: Quest) {
        if (quest !in _quests) {
            _quests.add(quest)
        }
        logMessage("You received quest ${quest.name}")
        QuestLogUpdated(this).emit()
    }


    fun remove(quest: Quest) = removeAt(_quests.indexOf(quest))

    fun removeAt(slot: Int): Quest {
        val quest = _quests[slot]
        _quests.removeAt(slot)
        return quest
    }

}