package cz.cuni.gamedev.nail123.roguelike.entities.NPCs

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.entities.quests.Quest
import cz.cuni.gamedev.nail123.roguelike.events.EnemyOrcDied
import cz.cuni.gamedev.nail123.roguelike.mechanics.Interaction
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Position3D
import org.hexworks.zircon.api.data.Size3D

class QuestGiver: NPC(GameTiles.HUMANHANDSUP) {
    override val blocksVision = false
    var quests = ArrayDeque<Quest>()

    fun addQuest(quest: Quest){
        quests.add(quest)
    }

    fun giveQuest() : Quest {
        quests.last().isTaken = true
        return quests.removeLast()
    }

    fun hasQuest() : Boolean {
        return quests.isNotEmpty()
    }

    fun giveReward(quest: Quest) {
        this.block.area.addAtEmptyPosition(
            quest.reward,
            Position3D.create(this.position.x - 1, this.position.y - 1, 0),
            Size3D.create(3, 3, 1)
        )
    }


    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) { player -> Interaction.takeQuest(player, this@QuestGiver) }
    }

    override fun interactWith(other: GameEntity, type: InteractionType) = false

}