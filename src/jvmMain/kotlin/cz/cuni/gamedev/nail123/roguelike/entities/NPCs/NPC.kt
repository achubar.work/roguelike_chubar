package cz.cuni.gamedev.nail123.roguelike.entities.NPCs

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.MovingEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.mechanics.Combat
import cz.cuni.gamedev.nail123.roguelike.mechanics.Interaction
import org.hexworks.zircon.api.data.Tile

abstract class NPC(tile: Tile): MovingEntity(tile), Interactable, Interacting {
    override val blocksMovement = true

}