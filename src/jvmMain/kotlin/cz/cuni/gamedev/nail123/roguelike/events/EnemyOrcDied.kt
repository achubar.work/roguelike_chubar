package cz.cuni.gamedev.nail123.roguelike.events

import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Orc

class EnemyOrcDied(override val emitter: Orc): GameEvent() {}