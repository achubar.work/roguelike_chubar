package cz.cuni.gamedev.nail123.roguelike.blocks

import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class PathFloor: GameBlock(GameTiles.FLOOR){
    override val blocksVision = true
}