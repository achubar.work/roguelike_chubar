package cz.cuni.gamedev.nail123.roguelike.entities.quests

import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Orc
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item
import cz.cuni.gamedev.nail123.roguelike.events.EnemyOrcDied
import cz.cuni.gamedev.nail123.roguelike.events.EnemyRatDied
import cz.cuni.gamedev.nail123.roguelike.events.InventoryUpdated
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import org.hexworks.cobalt.events.api.CallbackResult
import org.hexworks.cobalt.events.api.KeepSubscription
import org.hexworks.zircon.internal.Zircon

class KillEnemies(var targetKills: Int, var targetEnemy: String, override val reward: Item) : Quest(reward) {
    var currentKills = 0

    init { // This is not a nice implementation, but I am not familiar with Zircon and Kotlin
        when (targetEnemy) {
            "Orc" -> {
                Zircon.eventBus.subscribeTo<EnemyOrcDied>(key = "EnemyOrcDied") {
                    if (currentKills < targetKills && isTaken)
                    {
                        currentKills++
                        if (currentKills == targetKills)
                        {
                            logMessage("You completed the quest. Find the Quest Giver")
                            completed = true
                        }
                    }

                    KeepSubscription
                }
            }
            "Rat" -> {
                Zircon.eventBus.subscribeTo<EnemyRatDied>(key = "EnemyRatDied") {
                    if (currentKills < targetKills && isTaken)
                    {
                        currentKills++
                        if (currentKills == targetKills)
                        {
                            logMessage("You completed the quest. Find the Quest Giver")
                            completed = true
                        }
                    }

                    KeepSubscription
                }
            }
        }

    }

    override val name: String
        get() = "Kill " + targetKills + ' ' + targetEnemy + 's'

    override val progress: String
        get() = "$currentKills/$targetKills"

}