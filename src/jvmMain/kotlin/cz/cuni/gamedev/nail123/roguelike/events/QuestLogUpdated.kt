package cz.cuni.gamedev.nail123.roguelike.events

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.QuestLog

class QuestLogUpdated(override val emitter: QuestLog): GameEvent() {}