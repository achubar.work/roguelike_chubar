package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.GameConfig;
import cz.cuni.gamedev.nail123.roguelike.blocks.*;
import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Door;
import cz.cuni.gamedev.nail123.roguelike.entities.quests.Quest;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import cz.cuni.gamedev.nail123.utils.collections.ObservableMap;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Stack;

public class MyAreaBuilder extends AreaBuilder {

    public MyAreaBuilder(Size3D size, Size3D visibleSize){
        super(size, visibleSize);
    }

    public MyAreaBuilder(){
        super(GameConfig.INSTANCE.getAREA_SIZE(), GameConfig.INSTANCE.getVISIBLE_SIZE());
    }

    SplitTree spaceSplitTree;

    @NotNull
    @Override
    public AreaBuilder create() {
        int depth = 3; // Best with 2 and 3
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                getBlocks().put(Position3D.create(i, j, 0), new Black());
            }
        }
        spaceSplitTree = new SplitTree(depth, this.getSize(), this); // Creating the structure to fill it with corridors and rooms
        spaceSplitTree.drawPaths(spaceSplitTree.root);
        spaceSplitTree.drawRooms(spaceSplitTree.root);
        return this;
    }

    public Position3D getEmptyRoomPos(Boolean isLeft) { // This is useless
        return spaceSplitTree.getEmptyRoomPos(isLeft, 5);
    }

}

class Node{
    Position3D pos;
    Size3D size;
    Node left;
    Node right;
    Boolean empty;
    Position3D center;
    Node(Position3D pos, Size3D size) {
        this.pos = pos;
        empty = true;
        right = null;
        left = null;
        this.size = size;
        center = Position3D.create(pos.getX() + size.getXLength()/2, pos.getY() + size.getYLength()/2, 0);
    }
}

class SplitTree{
    Node root;

    AreaBuilder area;

    SplitTree(int depth, Size3D size, AreaBuilder area){
        root = new Node(Position3D.create(0, 0, 0), size);
        this.area = area;
        splitTreeRecursive(root, depth);
    }

    Position3D getEmptyRoomPos(Boolean isLeft, int attempts) {
        ArrayList<Node> rooms = new ArrayList<>();
        Node node;
        if (isLeft)
        {
            node = root.left;
        }
        else
        {
            node = root.right;
        }
        while(node != null)
        {
            rooms.add(node);
            if(random(0,1) == 0)
            {
                node = node.left;
            }
            else
            {
                node = node.right;
            }
        }
        for (int i = 0; i < rooms.size(); i++)
        {
            if (rooms.get(i).empty)
            {
                return rooms.get(i).center;
            }
        }
        if (attempts > 0)
        {
            return getEmptyRoomPos(isLeft, attempts--);
        }
        return null;
    }

    void splitTreeRecursive(Node node, int depth)
    {

        int splitPointX;
        int splitPointY;
        int leftWidth;
        int leftHeight;
        double vRatio = (double)node.size.getYLength() / (double)node.size.getXLength();
        double hRatio = (double)node.size.getXLength() / (double)node.size.getYLength();
        if (((random(0, 1) == 1) && hRatio > 0.75) || vRatio < 0.75) // Vertical
        {
            splitPointX = random(node.size.getXLength()*3/8, node.size.getXLength()*5/8); // Areas are split close to the middle point
            splitPointY = 0;
            leftWidth = splitPointX;
            leftHeight = node.size.getYLength();
        }
        else // Horizontal
        {
            splitPointX = 0;
            splitPointY = random(node.size.getYLength()*3/8, node.size.getYLength()*5/8);
            leftWidth = node.size.getXLength();
            leftHeight = splitPointY;
        }

        node.left = new Node(Position3D.create(node.pos.getX(), node.pos.getY(), 0),
                Size3D.create(leftWidth, leftHeight, 1));
        node.right = new Node(Position3D.create(node.pos.getX() + splitPointX, node.pos.getY() + splitPointY, 0),
                Size3D.create(node.size.getXLength() - splitPointX, node.size.getYLength() - splitPointY, 1));

        if (node.left.size.getYLength() < 7 || node.left.size.getXLength() < 7 || node.right.size.getXLength() < 7 || node.right.size.getYLength() < 7)
        { // If the result of splitting is too small we stop it
            node.left = null;
            node.right = null;
            return;
        }
        if (depth < 1)
        {
            return;
        }

        splitTreeRecursive(node.left, depth - 1);
        splitTreeRecursive(node.right, depth - 1);
    }

    void drawRooms(Node node)
    {
        if (node.left == null || node.right == null)
        {
            drawRoom(node);
            return;
        }
        drawRooms(node.left);
        drawRooms(node.right);
    }

    void drawPaths(Node node) // This might be improved with the following idea:
    {                         // Add paths for each tree leaf in order from the leftmost to the rightmost
        if (node.left == null || node.right == null)
            return;

        Position3D rightCenter = Position3D.create(node.right.pos.getX() + node.right.size.getXLength()/2,
                node.right.pos.getY() + node.right.size.getYLength()/2, 0);
        Position3D leftCenter = Position3D.create(node.left.pos.getX() + node.left.size.getXLength()/2,
                node.left.pos.getY() + node.left.size.getYLength()/2, 0);

        Position3D firstBlock = Position3D.create(0, 0, 0);
        Position3D secondBlock = Position3D.create(0, 0, 0);

        if (rightCenter.getX() == leftCenter.getX()) // Horizontal path case
        {
            for(int i = leftCenter.getY(); i <= rightCenter.getY(); i++)
            {
                area.getBlocks().put(Position3D.create(leftCenter.getX(), i, 0), new PathFloor());
                firstBlock = Position3D.create(leftCenter.getX() + 1, i, 0);
                secondBlock = Position3D.create(leftCenter.getX() - 1, i, 0);

                if (!(area.getBlocks().get(firstBlock) instanceof PathFloor)) // Add walls if there is no intersection with a PathFloor
                    area.getBlocks().put(firstBlock, new Wall());

                if (!(area.getBlocks().get(secondBlock) instanceof PathFloor))
                    area.getBlocks().put(secondBlock, new Wall());
            }
        }
        else
        {
            for(int i = leftCenter.getX(); i <= rightCenter.getX(); i++) // Vertical path case
            {
                area.getBlocks().put(Position3D.create(i, leftCenter.getY(), 0), new PathFloor());
                firstBlock = Position3D.create(i, leftCenter.getY() + 1, 0);
                secondBlock = Position3D.create(i, leftCenter.getY() - 1, 0);

                if (!(area.getBlocks().get(firstBlock) instanceof PathFloor))
                    area.getBlocks().put(firstBlock, new Wall());

                if (!(area.getBlocks().get(secondBlock) instanceof PathFloor))
                    area.getBlocks().put(secondBlock, new Wall());
            }
        }
        drawPaths(node.left);
        drawPaths(node.right);
    }


    void drawRoom(Node node){

        int leftMargin = random(1, node.size.getXLength()/4); // Margin between wall and tree leaf borders
        int rightMargin = random(1, node.size.getXLength()/4);
        int bottomMargin = random(1, node.size.getYLength()/4);
        int topMargin = random(1, node.size.getYLength()/4);

        boolean leftPlaced = false; //only one door is placed in case of intersection of corridor and each room wall
        boolean rightPlaced = false;
        boolean bottomPlaced = false;
        boolean topPlaced = false;

        for (int i = node.pos.getX() + leftMargin; i < node.pos.getX() + node.size.getXLength() - rightMargin; i++)
        {
            Position3D firstBlock = Position3D.create(i, node.pos.getY() + topMargin, 0); // Represents top wall
            Position3D secondBlock = Position3D.create(i, node.pos.getY() + node.size.getYLength() - bottomMargin - 1, 0); // Represents bottom wall

            // Top door
            if (area.getBlocks().get(firstBlock) instanceof PathFloor) // Check if wall intersect with a corridor
            {   // Here I make sure that I delete all unnecessary doors
                area.getBlocks().get(firstBlock).getEntities().clear();
                if (!topPlaced) // Here we check if there is already a door in the wall (top in this case)
                {               // if not, we leave the PathFloor here.
                    topPlaced = true;
                    area.addEntity(new Door(), firstBlock);
                }
            }
            else
                area.getBlocks().put(firstBlock, new Wall());
            // After that we repeat the same with 3 other walls
            // Bottom wall
            if (area.getBlocks().get(secondBlock) instanceof PathFloor)
            {
                area.getBlocks().get(secondBlock).getEntities().clear();
                if (!bottomPlaced)
                {
                    bottomPlaced = true;
                    area.addEntity(new Door(), secondBlock);
                }
            }
            else
                area.getBlocks().put(secondBlock, new Wall());
        }
        for (int i = node.pos.getY() + topMargin; i < node.pos.getY() + node.size.getYLength() - bottomMargin; i++)
        {
            Position3D firstBlock = Position3D.create(node.pos.getX() + leftMargin, i, 0); // Represents left wall
            Position3D secondBlock = Position3D.create(node.pos.getX() + node.size.getXLength() - rightMargin - 1, i, 0); // Represents right wall

            // Left wall
            if (area.getBlocks().get(firstBlock) instanceof PathFloor)
            {
                area.getBlocks().get(firstBlock).getEntities().clear();
                if (!leftPlaced)
                {
                    leftPlaced = true;
                    area.addEntity(new Door(), firstBlock);
                }
            }
            else
                area.getBlocks().put(firstBlock, new Wall());
            // Right wall
            if (area.getBlocks().get(secondBlock) instanceof PathFloor)
            {
                area.getBlocks().get(secondBlock).getEntities().clear();
                if (!rightPlaced)
                {
                    rightPlaced = true;
                    area.addEntity(new Door(), secondBlock);
                }
            }
            else
                area.getBlocks().put(secondBlock, new Wall());
        }
        for (int i = node.pos.getX() + leftMargin + 1; i < node.pos.getX() + node.size.getXLength() - rightMargin - 1; i++) // Filling the room with a floor
        {
            for (int j = node.pos.getY() + topMargin + 1; j < node.pos.getY() + node.size.getYLength() - bottomMargin - 1; j++)
            {
                area.getBlocks().put(Position3D.create(i, j, 0), new Floor());
            }
        }
    }

    int random(int min, int max){
        return (int)Math.floor(Math.random()*(max - min + 1) + min);
    } //Function to receive a random number in range
}
