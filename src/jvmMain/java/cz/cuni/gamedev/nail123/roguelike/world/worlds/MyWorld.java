package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.entities.NPCs.QuestGiver;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Chest;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Weapon;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs;
import cz.cuni.gamedev.nail123.roguelike.entities.quests.KillEnemies;
import cz.cuni.gamedev.nail123.roguelike.entities.quests.Quest;
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent;
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding;
import cz.cuni.gamedev.nail123.roguelike.world.Area;
import cz.cuni.gamedev.nail123.roguelike.world.World;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Orc;
import cz.cuni.gamedev.nail123.utils.collections.ObservableList;
import kotlin.random.Random;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class MyWorld extends World {
    int currentLevel = 0;

    ObservableList<Area> levels;

    private QuestGiver questGiver;
    public MyWorld() {
        levels = getAreas();
    }

    @NotNull
    @Override
    public Area buildStartingArea() {
        return buildLevel(0);
    }

    private int random(int min, int max){
        return (int)Math.floor(Math.random()*(max - min + 1) + min);
    }

    private Quest getRandomQuest(){ // Can be extended by adding new types of quests
        int numKills = random(2 + currentLevel/3, 5 + currentLevel/3);
        String enemy;
        if (random(0, 1) == 0)
            enemy = "Orc";
        else
            enemy = "Rat";
        return new KillEnemies(numKills, enemy, getRandomItem((currentLevel + numKills)/5.0 + 1.0)); // it grows too fast
    }

    private Item getRandomItem(double difficultyModificator){
        if (random(0, 1) == 0)
        {
            int upperLimit = random(4, 7);
            int rnd = random(0, upperLimit);
            int damage = (int)((4 + rnd) * difficultyModificator);
            double leechOnKill = 0.1 * (upperLimit - rnd) * difficultyModificator; // power of the weapon is still balanced
            return new Sword(damage, leechOnKill);
        }
        else
        {
            int upperLimit = random(2, 4);
            int rnd = random(0, upperLimit);
            int defense = (int)((1 + rnd) * difficultyModificator);
            int healOnLevel = (int)(5 * (upperLimit - rnd) * difficultyModificator);
            return new Chest(defense, healOnLevel);
        }
    }

    Area buildLevel(int floor) {
        // Start with an empty area
        AreaBuilder areaBuilder = (new MyAreaBuilder()).create();

        areaBuilder.addAtEmptyPosition(
                areaBuilder.getPlayer(),
                Position3D.create(1, 1, 0),
                Size3D.create(areaBuilder.getWidth() / 5 - 2, areaBuilder.getHeight() / 5 - 2, 1)
        );


        if (currentLevel == 0)
        {
            questGiver = new QuestGiver();// I tried to put it into constructor but the program didn't work in this case for some reason
            areaBuilder.addAtEmptyPosition(
                    getRandomItem(0.8),
                    Position3D.create(1, 1, 0),
                    Size3D.create(areaBuilder.getWidth() / 5 - 2, areaBuilder.getHeight() / 5 - 2, 1)
            );
        }
        if (currentLevel % 3 == 0)
        {
            questGiver.addQuest(getRandomQuest());
            areaBuilder.addAtEmptyPosition(
                    questGiver,
                    Position3D.create(1, 1, 0),
                    Size3D.create(areaBuilder.getWidth() / 4 - 2, areaBuilder.getHeight() / 4 - 2, 1)
            );
        }

        // Add stairs down
        Map<Position3D, Integer> floodFill = Pathfinding.INSTANCE.floodFill(
                areaBuilder.getPlayer().getPosition(),
                areaBuilder,
                Pathfinding.INSTANCE.getEightDirectional(),
                Pathfinding.INSTANCE.getDoorOpening()
        );

        Position3D[] exitPositions = {};
        exitPositions = floodFill.keySet().toArray(exitPositions);

        Position3D staircasePosition = exitPositions[Random.Default.nextInt(exitPositions.length - 50, exitPositions.length)]; // Randomly choose one from
                                                                                                                                    // the 50 farmost positions

        areaBuilder.addEntity(new Stairs(), staircasePosition);

        for (int i = 0; i <= currentLevel && i < 2; ++i) {
            areaBuilder.addAtEmptyPosition(new Orc(currentLevel), Position3D.defaultPosition(), areaBuilder.getSize());
            areaBuilder.addAtEmptyPosition(new Rat(currentLevel), Position3D.defaultPosition(), areaBuilder.getSize());
        }


        // Build it into a full Area
        return areaBuilder.build();
    }

    /**
     * Moving down - goes to a brand new level.
     */
    @Override
    public void moveDown() {
        ++currentLevel;
        (new LoggedEvent(this, "Descended to level " + (currentLevel + 1))).emit();
        getPlayer().heal(getPlayer().getHealOnLevel());
        if (currentLevel >= getAreas().getSize()) levels.add(buildLevel(levels.getSize()));
        goToArea(levels.get(currentLevel));
    }

    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    @Override
    public void moveUp() {
        --currentLevel;
        (new LoggedEvent(this, "Ascended to level " + (currentLevel + 1))).emit();
        System.out.println("Going up to level" + (currentLevel + 1));
        goToArea(levels.get(currentLevel));
    }
}
